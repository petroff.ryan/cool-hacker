#include "aconfig.h"
#ifdef CACA_DRIVER
/*includes */
#include <ui.h>
#include <caca.h>

static int caca_set_color(int r, int g, int b, int init)
{
    return ( /*pixel value or -1 for full palette */ -1);
}

static void caca_setpalette(ui_palette pal, int start, int end)
{
}

static void caca_print(int x, int y, CONST char *text)
{
}

static void caca_display()
{
}

static void caca_flip_buffers()
{
}

void caca_free_buffers(char *b1, char *b2)
{
}

int caca_alloc_buffers(char **b1, char **b2, void **data)
{
    return 1; /* bytes per scanline */
}

static void caca_getsize(int *w, int *h)
{
}

static void
caca_processevents(int wait, int *mx, int *my, int *mb, int *k)
{
}

static int caca_init()
{
    caca_canvas_t *cv; caca_display_t *dp; caca_event_t ev;
    dp = caca_create_display(NULL);
    if(!dp) return 1;
    cv = caca_get_canvas(dp);
    caca_set_display_title(dp, "Hello!");
    caca_set_color_ansi(cv, CACA_BLACK, CACA_WHITE);
    caca_put_str(cv, 0, 0, "This is a victory message! :)");
    caca_refresh_display(dp);
    caca_get_event(dp, CACA_EVENT_KEY_PRESS, &ev, -1);
    caca_free_display(dp);

    return ( /*1 for sucess 0 for fail */ 1);
}

static void caca_uninitialise()
{
}

static void caca_getmouse(int *x, int *y, int *b)
{
}


static void caca_mousetype(int type)
{
}

static struct params params[] = {
    {"", P_HELP, NULL, "Caca driver options:"},
    {"-flag", P_SWITCH, &variable, "Caca flag..."},
    {NULL, 0, NULL, NULL}
};

struct ui_driver caca_driver = {
    "Caca",
    caca_init,
    caca_getsize,
    caca_processevents,
    caca_getmouse,
    caca_uninitialise,
    caca_set_color,		/*You should implement just one */
    caca_set_palette,	/*of these and add NULL as second */
    caca_print,
    caca_display,
    caca_alloc_buffers,
    caca_free_buffers,
    caca_flip_buffers,
    caca_mousetype,		/*This should be NULL */
    NULL,			/*flush */
    8,				/*text width */
    8,				/*text height */
    UGLYTEXTSIZE,
    params,
    0,				/*flags...see ui.h */
    0.0, 0.0,			/*width/height of screen in centimeters */
    0, 0,			/*resolution of screen for windowed systems */
    UI_C256,			/*Image type */
    0, 255, 255			/*start, end of palette and maximum allocatable */
	/*entries */
};

/* DONT FORGET TO ADD DOCUMENTATION ABOUT YOUR DRIVER INTO xaos.hlp FILE!*/

#endif
